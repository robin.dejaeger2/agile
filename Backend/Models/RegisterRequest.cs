﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models
{
    public class RegisterRequest
    {
        [Required]
        public string Naam { get; set; }

        [Required]
        public string Voornaam { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Paswoord { get; set; }

        [Required]
        public string Rol { get; set; }

        public string? Code { get; set; }

    }
}
