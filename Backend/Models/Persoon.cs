﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Bogus;
using System.Text.Json.Serialization;

namespace Backend.Models
{
    public class Persoon
    {
        public int Id { get; set; }

        public string Naam { get; set; }

        public string Voornaam { get; set; }

        public string Email { get; set; }

        public string Paswoord { get; set; }
        public string BehaaldeDoelstellingen { get; set; }
        public int? Stageplaats_id { get; set; }
        public int? Docent_id { get; set; }
        public int? Mentor_id { get; set; }
        public int? Student_id { get; set; }
        public static Faker<Persoon> FakeData { get; } =
            new Faker<Persoon>()
                .RuleFor(p => p.Id, f => f.IndexFaker)
                .RuleFor(p => p.Naam, f => f.Name.LastName())
                .RuleFor(p => p.Voornaam, f => f.Name.FirstName())
                .RuleFor(p => p.Email, f => f.Internet.Email())
                .RuleFor(p => p.Paswoord, f => f.Internet.Password())
                .RuleFor(p => p.BehaaldeDoelstellingen, "")
                .RuleFor(p => p.Stageplaats_id, f => f.IndexFaker)
                .RuleFor(p => p.Docent_id, f => null)
                .RuleFor(p => p.Mentor_id, f => null)
                .RuleFor(p => p.Student_id, f => f.IndexFaker);            

    }
}
