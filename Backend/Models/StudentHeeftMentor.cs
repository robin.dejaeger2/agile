﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Backend.Models
{
    public class StudentHeeftMentor
    {
        [Key]
        public int Student_id { get; set; }
        public int Mentor_id { get; set; }

    }
}
