﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Bogus;

namespace Backend.Models
{
    public class Stageplaats
    {
        private static int userId = 100;

        public int Id { get; set; }

        public string Straat { get; set; }

        public string Huisnummer { get; set; }

        public string Gemeente { get; set; }

        public string Postcode { get; set; }
        public string Gsm { get; set; }
        public string Naam { get; set; }

        public static Faker<Stageplaats> FakeData { get; } =
            new Faker<Stageplaats>()
                .RuleFor(p => p.Id, f => userId++)
                .RuleFor(p => p.Straat, f => f.Address.StreetName())
                .RuleFor(p => p.Huisnummer, f => f.Address.BuildingNumber())
                .RuleFor(p => p.Gemeente, f => f.Address.City())
                .RuleFor(p => p.Postcode, (f, p) => f.Address.ZipCode())
                .RuleFor(p => p.Gsm, (f, p) => f.Address.ZipCode())
                .RuleFor(p => p.Naam, (f, p) => f.Person.FirstName);
    }
}
