﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Bogus;

namespace Backend.Models
{
    public class Taak
    {
        public int Id { get; set; }

        public string Titel { get; set; }

        public string Beschrijving { get; set; }
        public string Categorie { get; set; }

        public DateTime Datum { get; set; }

        public int? Duur { get; set; }

        public int Persoon_id { get; set; }

        public static Faker<Taak> FakeData { get; } =
            new Faker<Taak>()
                .RuleFor(p => p.Id, f => f.IndexFaker)
                .RuleFor(p => p.Titel, f => f.Name.LastName())
                .RuleFor(p => p.Categorie, f => f.Name.LastName())
                .RuleFor(p => p.Datum, f => new DateTime())
                .RuleFor(p => p.Duur, f => f.IndexFaker)
                .RuleFor(p => p.Persoon_id, f => f.IndexFaker);
    }
}
