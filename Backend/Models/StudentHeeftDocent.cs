﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Bogus;

namespace Backend.Models
{
    public class StudentHeeftDocent
    {
        [Key]
        public int Student_id { get; set; }
        public int Docent_id { get; set; }
        public static Faker<StudentHeeftDocent> FakeData { get; } =
            new Faker<StudentHeeftDocent>()
                .RuleFor(p => p.Student_id, f => f.IndexFaker)
                .RuleFor(p => p.Docent_id, f => f.IndexFaker);
    }
}
