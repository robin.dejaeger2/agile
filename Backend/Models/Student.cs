﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Bogus;
using Newtonsoft.Json;
using Pomelo.EntityFrameworkCore.MySql.Storage;

namespace Backend.Models
{
    public class Student
    {
        public int Id { get; set; }

        public static Faker<Student> FakeData { get; } =
            new Faker<Student>()
                .RuleFor(p => p.Id, f => f.IndexFaker);
    }
}
