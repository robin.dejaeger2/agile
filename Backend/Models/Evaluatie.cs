﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Bogus;

namespace Backend.Models
{
    public class Evaluatie
    {
        private static int userId = 100;

        public int Id { get; set; }

        public int Complexiteit { get; set; }

        public int Begeleiding { get; set; }

        public int Voldoening { get; set; }

        public string Feedback { get; set; }

        public int Taak_id { get; set; }

        public static Faker<Evaluatie> FakeData { get; } =
            new Faker<Evaluatie>()
                .RuleFor(p => p.Id, f => userId++)
                .RuleFor(p => p.Complexiteit, f => f.Random.Number(0,3))
                .RuleFor(p => p.Begeleiding, f => f.Random.Number(0, 3))
                .RuleFor(p => p.Voldoening, f => f.Random.Number(0, 3))
                .RuleFor(p => p.Feedback, f => f.Name.FirstName())
                .RuleFor(p => p.Taak_id, f => f.IndexFaker);
    }
}
