﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models
{
    public class RequestStudentDocent
    {
        public int Student_id { get; set; }
        public int Docent_id { get; set; }
        public int Persoon_id { get; set; }
        public static Faker<RequestStudentDocent> FakeData { get; } =
            new Faker<RequestStudentDocent>()
                .RuleFor(p => p.Student_id, f => f.IndexFaker)
                .RuleFor(p => p.Docent_id, f => f.IndexFaker);
    }
}
