﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models
{
    public class SSORequest
    {
        [Required]
        public string Naam { get; set; }

        [Required]
        public string Voornaam { get; set; }
        [Required]
        public string Email { get; set; }

    }
}
