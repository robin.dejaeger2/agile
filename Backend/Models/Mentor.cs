﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Bogus;

namespace Backend.Models
{
    public class Mentor
    {
        private static int userId = 100;
        public int Id { get; set; }
        public static Faker<Mentor> FakeData { get; } =
            new Faker<Mentor>()
                .RuleFor(p => p.Id, f => userId++);
    }
}
