﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models
{
    public class StageplaatsRequest
    {
        public int Id { get; set; }

        public string Straat { get; set; }

        public string Huisnummer { get; set; }

        public string Gemeente { get; set; }

        public string Postcode { get; set; }
        public string Gsm { get; set; }
        public string Naam { get; set; }

        public int Persoon_Id { get; set; }
    }
}
