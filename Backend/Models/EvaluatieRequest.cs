﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models
{
    public class EvaluatieRequest
    {
        public int Id { get; set; }

        public int Complexiteit { get; set; }

        public int Begeleiding { get; set; }

        public int Voldoening { get; set; }

        public string Feedback { get; set; }

        public int Taak_id { get; set; }

        public int? Duur { get; set; }
        public static Faker<EvaluatieRequest> FakeData { get; } =
            new Faker<EvaluatieRequest>()
                .RuleFor(p => p.Id, f => f.IndexFaker)
                .RuleFor(p => p.Complexiteit, f => f.Random.Number(0, 3))
                .RuleFor(p => p.Begeleiding, f => f.Random.Number(0, 3))
                .RuleFor(p => p.Voldoening, f => f.Random.Number(0, 3))
                .RuleFor(p => p.Feedback, f => f.Name.FirstName())
                .RuleFor(p => p.Taak_id, f => f.IndexFaker)
                .RuleFor(p => p.Duur, f => f.IndexFaker);
    }
}
