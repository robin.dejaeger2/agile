﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models
{
    public class RequestStudentMentor
    {
        public int Student_id { get; set; }
        public int Mentor_id { get; set; }
        public int Persoon_id { get; set; }
        public static Faker<RequestStudentMentor> FakeData { get; } =
            new Faker<RequestStudentMentor>()
                .RuleFor(p => p.Student_id, f => f.IndexFaker)
                .RuleFor(p => p.Mentor_id, f => f.IndexFaker);
    }
}
