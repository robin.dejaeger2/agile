﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models
{
    public class Doelstelling
    {
        public int Id { get; set; }
        public string Doel { get; set; }
        public static Faker<Doelstelling> FakeData { get; } =
            new Faker<Doelstelling>()
                .RuleFor(p => p.Id, f => f.IndexFaker)
                .RuleFor(p => p.Doel, f => f.Name.FirstName());
    }
}
