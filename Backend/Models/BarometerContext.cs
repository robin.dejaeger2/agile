﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;
using Backend.Models;

namespace Backend.Models
{
    public class BarometerContext : DbContext
    {
        //https://www.youtube.com/watch?v=XyuUEy3szt8&t=207s
        public DbSet<Student> Student { get; set; }
        public DbSet<Docent> Docent { get; set; }
        public DbSet<Mentor> Mentor { get; set; }
        public DbSet<Stageplaats> Stageplaats { get; set; }
        public DbSet<Persoon> Persoon { get; set; }
        public DbSet<Taak> Taak { get; set; }
        public DbSet<Evaluatie> Evaluatie { get; set; }
        public DbSet<Categorie> Categorie { get; set; }
        public DbSet<StudentHeeftDocent> Student_Heeft_Docent { get; set; }
        public DbSet<StudentHeeftMentor> Student_Heeft_Mentor { get; set; }
        public DbSet<Secret> Code { get; set; }
        public DbSet<Doelstelling> Doelstelling { get; set; }

        public BarometerContext(DbContextOptions<BarometerContext> options) : base(options)
        {
        }   
    }
}
