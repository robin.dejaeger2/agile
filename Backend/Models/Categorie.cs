﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Bogus;

namespace Backend.Models
{
    public class Categorie
    {
        private static int userId = 100;

        public int Id { get; set; }

        public string Naam { get; set; }

        public string Kleur { get; set; }
        public static Faker<Categorie> FakeData { get; } =
            new Faker<Categorie>()
                .RuleFor(p => p.Id, f => userId++)
                .RuleFor(p => p.Naam, f => f.Name.FirstName())
                .RuleFor(p => p.Kleur, f => "green");
    }
}
