﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models
{
    public class AuthenticateResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string BehaaldeDoelstellingen { get; set; }
        public int? Stageplaats_id { get; set; }
        public int? Student_id { get; set; }
        public int? Docent_id { get; set; }
        public int? Mentor_id { get; set; }

        public string Token { get; set; }

        public AuthenticateResponse(Persoon persoon, string token)
        {
            Id = persoon.Id;
            FirstName = persoon.Voornaam;
            LastName = persoon.Naam;
            Email = persoon.Email;
            Role = persoon.Student_id == null ? (persoon.Docent_id == null ? "Mentor" : "Docent") : "Student";
            BehaaldeDoelstellingen = persoon.BehaaldeDoelstellingen;
            Stageplaats_id = persoon.Stageplaats_id;
            Student_id = persoon.Student_id;
            Docent_id = persoon.Docent_id;
            Mentor_id = persoon.Mentor_id;
            Token = token;
        }
    }
}
