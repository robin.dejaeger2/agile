﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Bogus;

namespace Backend.Models
{
    public class Docent
    {
        private static int userId = 100;

        public int Id { get; set; }
        public static Faker<Docent> FakeData { get; } =
            new Faker<Docent>()
                .RuleFor(p => p.Id, f => userId++);
    }
}
