﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models
{
    public class Secret
    {
        public int Id { get; set; }

        public string Code { get; set; }
    }
}
