﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Backend.Models;
using Backend.Services;
using Backend.Helpers;

namespace Backend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EvaluatiesController : ControllerBase
    {
        private readonly IEvaluatieService _evaluatieService;

        public EvaluatiesController(IEvaluatieService evaluatieService)
        {
            _evaluatieService = evaluatieService;
        }

        // GET: api/Evaluaties
        [HttpGet]
        public IEnumerable<Evaluatie> GetEvaluatie()
        {
            return _evaluatieService.AlleEvaluaties();
        }

        // GET: api/Evaluaties/5
        [HttpGet("{id}")]
        public ActionResult<Evaluatie> GetEvaluatie(int id)
        {
            if (!EvaluatieExists(id))
            {
                return BadRequest("Evaluatie does not exist");
            }

            var evaluatie = _evaluatieService.VindEvaluatie(id);

            if (evaluatie == null)
            {
                return NotFound();
            }

            return evaluatie;
        }

        // PUT: api/Evaluaties/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public IActionResult PutEvaluatie(int id, Evaluatie evaluatie)
        {
            if (id != evaluatie.Id)
            {
                return BadRequest("The id does not belong to given evaluatie");
            }

            if (!EvaluatieExists(id))
            {
                return BadRequest("Evaluatie does not exist");
            }

            try
            {
                _evaluatieService.PutEvaluatie(id, evaluatie);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }

        // POST: api/Evaluaties
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<object> PostEvaluatie(EvaluatieRequest evaluatie)
        {
            try
            {
                var response = await _evaluatieService.PostEvaluatie(evaluatie);
                if (response == null)
                {
                    return BadRequest();
                }
                else
                {
                    return Ok(response);
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }            
        }

        // DELETE: api/Evaluaties/5
        [HttpDelete("{id}")]
        public IActionResult DeleteEvaluatie(int id)
        {
            var response = GetEvaluatie(id);
            var evaluatie = response.Value;

            if (evaluatie != null)
            {
                if (id != evaluatie.Id)
                {
                    return BadRequest("Id is not the same as the categorie that was found");
                }
            }


            try
            {
                _evaluatieService.DeleteEvaluatie(id);
            }
            catch (Exception e)
            {
                if (!EvaluatieExists(id))
                {
                    return NotFound("Evaluatie was not found");
                }
                else
                {
                    return BadRequest(e.Message);
                }
            }

            return NoContent();
        }

        private bool EvaluatieExists(int id)
        {
            return _evaluatieService.EvaluatieExists(id);
        }
    }
}
