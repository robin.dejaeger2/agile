﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Backend.Models;
using MySql.Data.MySqlClient;
using System.Configuration;
using Backend.Services;
using Backend.Helpers;

namespace Backend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PersonenController : ControllerBase
    {
        private readonly IPersoonService _persoonService;

        public PersonenController(IPersoonService persoonService)
        {
            _persoonService = persoonService;
        }

        // GET: api/Persoons
        [HttpGet]
        public IEnumerable<Persoon> GetPersoon()
        {
            return _persoonService.AllePersonen();
        }

        // GET: api/Persoons/5
        [HttpGet("{id}")]
        public ActionResult<Persoon> GetPersoon(int id)
        {
            if (!PersoonExists(id))
            {
                return BadRequest("Persoon does not exist");
            }

            var persoon = _persoonService.VindPersoon(id);

            if (persoon == null)
            {
                return NotFound();
            }

            return persoon;
        }

        // PUT: api/Persoons/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public ActionResult PutPersoon(int id, Persoon persoon)
        {
            if (id != persoon.Id)
            {
                return BadRequest("The id does not belong to given persoon");
            }

            if (!PersoonExists(id))
            {
                return BadRequest("Persoon does not exist");
            }

            try
            {
                _persoonService.PutPersoon(id, persoon);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }

        // POST: api/Persoons
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public ActionResult PostPersoon(Persoon persoon)
        {
            try
            {
                _persoonService.PostPersoon(persoon);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }

        // DELETE: api/Persoons/5
        [HttpDelete("{id}")]
        public ActionResult DeletePersoon(int id)
        {
            var response = GetPersoon(id);
            var persoon = response.Value;

            if (persoon != null)
            {
                if (id != persoon.Id)
                {
                    return BadRequest("Id is not the same as the categorie that was found");
                }
            }


            try
            {
                _persoonService.DeletePersoon(id);
            }
            catch (Exception e)
            {
                if (!PersoonExists(id))
                {
                    return NotFound("Persoon was not found");
                }
                else
                {
                    return BadRequest(e.Message);
                }
            }

            return NoContent();
        }

        private bool PersoonExists(int id)
        {
            return _persoonService.PersoonExists(id);
        }
    }
}
