﻿using Backend.Helpers;
using Backend.Models;
using Backend.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Authorize]
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class StudentRelatieController : ControllerBase
    {
        private IStudentRelatieService _studentRelatieService;
        private ILoginService _loginService;

        public StudentRelatieController(IStudentRelatieService studentRelatieService, ILoginService loginService)
        {
            _studentRelatieService = studentRelatieService;
            _loginService = loginService;
        }

        [HttpPost]
        public async Task<IActionResult> Docent(RequestStudentDocent relatie)
        {
            try
            {
                var persoon = await _studentRelatieService.PostRelatieDocent(relatie);
                return await new LoginController(_loginService).GetPersonInfo(persoon);
            } catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Mentor(RequestStudentMentor relatie)
        {
            try
            {
                var persoon = await _studentRelatieService.PostRelatieMentor(relatie);
                return await new LoginController(_loginService).GetPersonInfo(persoon);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
