﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Backend.Models;
using Backend.Services;
using Microsoft.AspNetCore.Cors;
using Backend.Helpers;

namespace Backend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CategorienController : ControllerBase
    {
        private readonly ICategorieService _categorieService;

        public CategorienController(ICategorieService categorieService)
        {
            _categorieService = categorieService;
        }

        // GET: api/Categorien
        [HttpGet]
        public IEnumerable<Categorie> GetCategorie()
        {
            return _categorieService.AlleCategorien();
        }

        // GET: api/Categorien/5
        [HttpGet("{id}")]
        public ActionResult<Categorie> GetCategorie(int id)
        {
            if (!CategorieExists(id))
            {
                return BadRequest("Categorie does not exist");
            }

            var categorie = _categorieService.VindCategorie(id);

            if (categorie == null)
            {
                return NotFound();
            }

            return categorie;
        }

        // PUT: api/Categorien/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public IActionResult PutCategorie(int id, Categorie categorie)
        {
            if (id != categorie.Id)
            {
                return BadRequest("The id does not belong to given categorie");
            }

            if(!CategorieExists(id))
            {
                return BadRequest("Categorie does not exist");
            }

            try
            {
                _categorieService.PutCategorie(id, categorie);
                return NoContent();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // POST: api/Categorien
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public ActionResult PostCategorie(Categorie categorie)
        {
            try
            {
                _categorieService.PostCategorie(categorie);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }

        // DELETE: api/Categorien/5
        [HttpDelete("{id}")]
        public IActionResult DeleteCategorie(int id)
        {
            var response = GetCategorie(id);
            var categorie = response.Value;

            if (categorie != null)
            {
                if (id != categorie.Id)
                {
                    return BadRequest("Id is not the same as the categorie that was found");
                }
            }

            try
            {
                _categorieService.DeleteCategorie(id);
                return NoContent();
            }
            catch (Exception e)
            {
                if (!CategorieExists(id))
                {
                    return NotFound("Categorie was not found");
                }
                else
                {
                    return BadRequest(e.Message);
                }
            }
        }

        private bool CategorieExists(int id)
        {
            return _categorieService.CategorieExists(id);
        }
    }
}
