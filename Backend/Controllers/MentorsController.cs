﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Backend.Models;
using Backend.Services;
using Backend.Helpers;

namespace Backend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MentorsController : ControllerBase
    {
        private readonly IMentorService _mentorService;

        public MentorsController(IMentorService mentorService)
        {
            _mentorService = mentorService;
        }

        // GET: api/Mentors
        [HttpGet]
        public IEnumerable<Mentor> GetMentor()
        {
            return _mentorService.AlleMentors();
        }

        // GET: api/Mentors/5
        [HttpGet("{id}")]
        public ActionResult<Mentor> GetMentor(int id)
        {
            if (!MentorExists(id))
            {
                return BadRequest("Mentor does not exist");
            }

            var mentor = _mentorService.VindMentor(id);

            if (mentor == null)
            {
                return NotFound();
            }

            return mentor;
        }

        // PUT: api/Mentors/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public ActionResult PutMentor(int id, Mentor mentor)
        {
            if (id != mentor.Id)
            {
                return BadRequest();
            }

            if (!MentorExists(id))
            {
                return BadRequest("Mentor does not exist");
            }

            try
            {
                _mentorService.PutMentor(id, mentor);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }

        // POST: api/Mentors
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public ActionResult PostMentor(Mentor mentor)
        {
            try
            {
                _mentorService.PostMentor(mentor);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }

        // DELETE: api/Mentors/5
        [HttpDelete("{id}")]
        public ActionResult DeleteMentor(int id)
        {
            var response = GetMentor(id);
            var mentor = response.Value;

            if (mentor != null)
            {
                if (id != mentor.Id)
                {
                    return BadRequest("Id is not the same as the mentor that was found");
                }
            }

            try
            {
                _mentorService.DeleteMentor(id);
            }
            catch (Exception e)
            {
                if (!MentorExists(id))
                {
                    return NotFound("Mentor was not found");
                }
                else
                {
                    return BadRequest(e.Message);
                }
            }

            return NoContent();
        }

        private bool MentorExists(int id)
        {
            return _mentorService.MentorExists(id);
        }
    }
}
