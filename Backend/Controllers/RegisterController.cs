﻿using Backend.Models;
using Backend.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        private IRegisterService _registerService;
        private ILoginService _loginService;

        public RegisterController(IRegisterService registerService, ILoginService loginService)
        {
            _registerService = registerService;
            _loginService = loginService;
        }

        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate(RegisterRequest model)
        {
            try
            {
                var user = await _registerService.Register(model);
                if (user != null)
                {
                    return new LoginController(_loginService).Authenticate(new AuthenticateRequest { Email = user.Email, Paswoord = user.Paswoord });
                } else
                {
                    return BadRequest("something went wrong");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

    }
}
