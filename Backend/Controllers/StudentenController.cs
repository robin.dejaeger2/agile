﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Backend.Models;
using Backend.Services;
using Backend.Helpers;

namespace Backend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class StudentenController : ControllerBase
    {
        private readonly IStudentService _studentService;

        public StudentenController(IStudentService studentService)
        {
            _studentService = studentService;
        }

        // GET: api/Students
        [HttpGet]
        public IEnumerable<Student> GetStudent()
        {
            return _studentService.AlleStudenten();
        }

        // GET: api/Students/5
        [HttpGet("{id}")]
        public ActionResult<Student> GetStudent(int id)
        {
            if (!StudentExists(id))
            {
                return BadRequest("Student does not exist");
            }

            var student = _studentService.VindStudent(id);

            if (student == null)
            {
                return NotFound();
            }

            return student;
        }

        // PUT: api/Students/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /*[HttpPut("{id}")]
        public ActionResult PutStudent(int id, Student student)
        {
            if (id != student.Id)
            {
                return BadRequest();
            }

            if(!StudentExists(id))
            {
                return BadRequest("Student does not exist");
            }

            try
            {
                _studentService.PutStudent(id, student);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }*/

        // POST: api/Students
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public ActionResult PostStudent(Student student)
        {
            try
            {
                _studentService.PostStudent(student);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }

        // DELETE: api/Students/5
        [HttpDelete("{id}")]
        public ActionResult DeleteStudent(int id)
        {
            var response = GetStudent(id);
            var student = response.Value;

            if(student != null)
            {
                if (id != student.Id)
                {
                    return BadRequest("Id is not the same as the student that was found");
                }
            }
            

            try
            {
                _studentService.DeleteStudent(id);
            }
            catch (Exception e)
            {
                if (!StudentExists(id))
                {
                    return NotFound("Student was not found");
                }
                else
                {
                    return BadRequest(e.Message);
                }
            }

            return NoContent();
        }

        private bool StudentExists(int id)
        {
            return _studentService.StudentExists(id);
        }
    }
}
