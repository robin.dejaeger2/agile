﻿using Backend.Models;
using Backend.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private ILoginService _loginService;

        public LoginController(ILoginService loginService)
        {
            _loginService = loginService;
        }

        [HttpPost]
        public IActionResult Authenticate(AuthenticateRequest model)
        {
            dynamic response = new ExpandoObject();

            try
            {
                var persoon = _loginService.Authenticate(model);

                if (persoon == null)
                    return BadRequest(new { message = "Username or password is incorrect" });

                response.person = new
                {
                    id = persoon.Id,
                    name = persoon.FirstName + " " + persoon.LastName,
                    email = persoon.Email,
                    role = persoon.Role,
                    token = persoon.Token,
                    student = persoon.Student_id,
                    docent = persoon.Docent_id,
                    mentor = persoon.Mentor_id,
                    behaaldeDoelstellingen = persoon.BehaaldeDoelstellingen
                };

                if (persoon.Role.Equals("Student"))
                {
                    response.workplace = _loginService.GetStageplaats(persoon.Stageplaats_id);
                    response.mentors = _loginService.GetMentors(persoon.Student_id);
                    response.docents = _loginService.GetDocenten(persoon.Student_id);
                }
                else if (persoon.Role.Equals("Docent"))
                {
                    response.students = _loginService.GetStudentsOfDocent(persoon.Docent_id);
                }
                else
                {
                    response.students = _loginService.GetStudentsOfMentor(persoon.Mentor_id);
                }

                return Ok(response);
            } catch(Exception e)
            {
                return BadRequest(e.Message);
            }  
        }

        [HttpPost]
        public async Task<IActionResult> SSO(SSORequest model)
        {
            dynamic response = new ExpandoObject();

            try
            {
                var persoon = await _loginService.SSO(model);

                if (persoon == null)
                    return BadRequest(new { message = "Username or password is incorrect" });

                response.person = new
                {
                    id = persoon.Id,
                    name = persoon.FirstName + " " + persoon.LastName,
                    email = persoon.Email,
                    role = persoon.Role,
                    token = persoon.Token,
                    student = persoon.Student_id,
                    docent = persoon.Docent_id,
                    mentor = persoon.Mentor_id,
                    behaaldeDoelstellingen = persoon.BehaaldeDoelstellingen
                };

                if (persoon.Role.Equals("Student"))
                {
                    response.workplace = _loginService.GetStageplaats(persoon.Stageplaats_id);
                    response.mentors = _loginService.GetMentors(persoon.Student_id);
                    response.docents = _loginService.GetDocenten(persoon.Student_id);
                }
                else if (persoon.Role.Equals("Docent"))
                {
                    response.students = _loginService.GetStudentsOfDocent(persoon.Docent_id);
                }
                else
                {
                    response.students = _loginService.GetStudentsOfMentor(persoon.Mentor_id);
                }

                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }   
        }

        [HttpPost]
        public async Task<IActionResult> GetPersonInfo(int id)
        {
            dynamic response = new ExpandoObject();

            try
            {
                var persoon = await _loginService.GetPersonInfo(id);

                if (persoon == null)
                    return BadRequest(new { message = "Persoon not found" });

                response.person = new
                {
                    id = persoon.Id,
                    name = persoon.FirstName + " " + persoon.LastName,
                    email = persoon.Email,
                    role = persoon.Role,
                    token = persoon.Token,
                    student = persoon.Student_id,
                    docent = persoon.Docent_id,
                    mentor = persoon.Mentor_id,
                    behaaldeDoelstellingen = persoon.BehaaldeDoelstellingen
                };

                if (persoon.Role.Equals("Student"))
                {
                    response.workplace = _loginService.GetStageplaats(persoon.Stageplaats_id);
                    response.mentors = _loginService.GetMentors(persoon.Student_id);
                    response.docents = _loginService.GetDocenten(persoon.Student_id);
                }
                else if (persoon.Role.Equals("Docent"))
                {
                    response.students = _loginService.GetStudentsOfDocent(persoon.Docent_id);
                }
                else
                {
                    response.students = _loginService.GetStudentsOfMentor(persoon.Mentor_id);
                }

                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
