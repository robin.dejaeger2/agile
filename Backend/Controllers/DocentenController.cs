﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Backend.Models;
using Backend.Services;
using Backend.Helpers;

namespace Backend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DocentenController : ControllerBase
    {
        private readonly IDocentService _docentService;
        public DocentenController(IDocentService docentService)
        {
            _docentService = docentService;
        }

        // GET: api/Docents
        [HttpGet]
        public IEnumerable<Docent> GetDocent()
        {
            return _docentService.AlleDocenten();
        }

        // GET: api/Docents/5
        [HttpGet("{id}")]
        public ActionResult<Docent> GetDocent(int id)
        {
            if (!DocentExists(id))
            {
                return BadRequest("Docent does not exist");
            }

            var docent = _docentService.VindDocent(id);

            if (docent == null)
            {
                return NotFound();
            }

            return docent;
        }

        // PUT: api/Docents/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public ActionResult PutDocent(int id, Docent docent)
        {
            if (id != docent.Id)
            {
                return BadRequest("The id does not belong to given docent");
            }

            if (!DocentExists(id))
            {
                return BadRequest("Docent does not exist");
            }

            try
            {
                _docentService.PutDocent(id, docent);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }

        // POST: api/Docents
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public ActionResult PostDocent(Docent docent)
        {
            try
            {
                _docentService.PostDocent(docent);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }

        // DELETE: api/Docents/5
        [HttpDelete("{id}")]
        public IActionResult DeleteDocent(int id)
        {
            var response = GetDocent(id);
            var docent = response.Value;

            if (docent != null)
            {
                if (id != docent.Id)
                {
                    return BadRequest("Id is not the same as the categorie that was found");
                }
            }

            try
            {
                _docentService.DeleteDocent(id);
            }
            catch (Exception e)
            {
                if (!DocentExists(id))
                {
                    return NotFound("Docent was not found");
                }
                else
                {
                    return BadRequest(e.Message);
                }
            }

            return NoContent();
        }

        private bool DocentExists(int id)
        {
            return _docentService.DocentExists(id);
        }
    }
}
