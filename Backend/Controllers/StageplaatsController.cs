﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Backend.Models;
using Backend.Services;
using Backend.Helpers;

namespace Backend.Controllers
{
    [Authorize]
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class StageplaatsController : ControllerBase
    {
        private readonly IStagePlaatsService _stageplaatsService;

        public StageplaatsController(IStagePlaatsService stageplaatsService)
        {
            _stageplaatsService = stageplaatsService;
        }

        // GET: api/Stageplaats
        [HttpGet]
        public IEnumerable<Stageplaats> GetStageplaats()
        {
            return _stageplaatsService.AlleStageplaatsen();
        }

        // GET: api/Stageplaats/5
        [HttpGet("{id}")]
        public ActionResult<Stageplaats> GetStageplaats(int id)
        {
            if (!StageplaatsExists(id))
            {
                return BadRequest("Stageplaats does not exist");
            }

            var stageplaats = _stageplaatsService.FindStageplaats(id);

            if (stageplaats == null)
            {
                return NotFound();
            }

            return stageplaats;
        }

        // PUT: api/Stageplaats/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public ActionResult PutStageplaats(int id, Stageplaats stageplaats)
        {
            if (id != stageplaats.Id)
            {
                return BadRequest("The id does not belong to given stageplaats");
            }

            if (!StageplaatsExists(id))
            {
                return BadRequest("Stageplaats does not exist");
            }

            try
            {
                _stageplaatsService.PutStageplaats(id, stageplaats);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }

        // POST: api/Stageplaats
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public ActionResult PostStageplaats(Stageplaats stageplaats)
        {
            try
            {
                _stageplaatsService.PostStageplaats(stageplaats);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Stageplaats>> PostStageplaatsStudent(StageplaatsRequest model)
        {
            try
            {
                Stageplaats stageplaats = await _stageplaatsService.PostStageplaatsStudent(model);
                return stageplaats;
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        // DELETE: api/Stageplaats/5
        [HttpDelete("{id}")]
        public ActionResult DeleteStageplaats(int id)
        {
            var response = GetStageplaats(id);
            var stageplaats = response.Value;

            if (stageplaats != null)
            {
                if (id != stageplaats.Id)
                {
                    return BadRequest("Id is not the same as the categorie that was found");
                }
            }


            try
            {
                _stageplaatsService.DeleteStageplaats(id);
            }
            catch (Exception e)
            {
                if (!StageplaatsExists(id))
                {
                    return NotFound("Stageplaats was not found");
                }
                else
                {
                    return BadRequest(e.Message);
                }
            }

            return NoContent();
        }

        private bool StageplaatsExists(int id)
        {
            return _stageplaatsService.StageplaatsExists(id);
        }
    }
}
