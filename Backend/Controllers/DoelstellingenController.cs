﻿using Backend.Helpers;
using Backend.Models;
using Backend.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DoelstellingenController : ControllerBase
    {
        private readonly IDoelstellingService _doelstellingService;
        public DoelstellingenController(IDoelstellingService doelstellingService)
        {
            _doelstellingService = doelstellingService;
        }

        [HttpGet]
        public IEnumerable<Doelstelling> GetDoelstellingen()
        {
            return _doelstellingService.AlleDoelstellingen();
        }

        [HttpPut("{id}")]
        public ActionResult<string> PutDoelstellingen(int id, BehaaldeDoelstellingen doelstellingen)
        {
            if (!PersoonExists(id))
            {
                return BadRequest("Persoon does not exist");
            } 

            try
            {
                return _doelstellingService.PutDoelstellingen(id, doelstellingen);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }

        private bool PersoonExists(int id)
        {
            return _doelstellingService.PersoonExists(id);
        }
    }
}
