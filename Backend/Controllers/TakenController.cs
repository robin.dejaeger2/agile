﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Backend.Models;
using Backend.Services;
using System.Dynamic;
using Backend.Helpers;

namespace Backend.Controllers
{
    [Authorize]
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class TakenController : ControllerBase
    {
        private readonly ITaakService _taakService;

        public TakenController(ITaakService taakService)
        {
            _taakService = taakService;
        }

        [HttpGet("{id}")]
        public ActionResult<object> GetTaken(int id)
        {
            if (!PersoonExists(id))
            {
                return BadRequest("Persoon does not exist");
            }

            try {
                var result = _taakService.AlleTaken(id);

                return result;
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }  
        }

        // GET: api/Taken
        [HttpGet("{id}/{datum}")]
        public object GetTakenByDate(int id, string datum)
        {
            if (!PersoonExists(id))
            {
                return BadRequest("Persoon does not exist");
            }

            try
            {
                return _taakService.TakenStudent(id, datum);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        // GET: api/Taken/5
        [HttpGet("{id}")]
        public ActionResult<Taak> GetTaak(int id)
        {
            if (!TaakExists(id))
            {
                return BadRequest("Taak does not exist");
            }

            var taak = _taakService.VindTaak(id);

            if (taak == null)
            {
                return NotFound();
            }

            return taak;
        }

        // PUT: api/Taken/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public ActionResult PutTaak(int id, Taak taak)
        {
            if (id != taak.Id)
            {
                return BadRequest("The id does not belong to given taak");
            }

            if (!TaakExists(id))
            {
                return BadRequest("Taak does not exist");
            }

            try
            {
                _taakService.PutTaak(id, taak);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }

            // POST: api/Taken
            // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public ActionResult<Taak> PostTaak(Taak taak)
        {
            try
            {
                var response = _taakService.PostTaak(taak);

                return Ok(response);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }

        // DELETE: api/Taken/5
        [HttpDelete("{id}")]
        public IActionResult DeleteTaak(int id)
        {
            var response = GetTaak(id);
            var taak = response.Value;

            if (taak != null)
            {
                if (id != taak.Id)
                {
                    return BadRequest("Id is not the same as the categorie that was found");
                }
            }


            try
            {
                _taakService.DeleteTaak(id);
            }
            catch (Exception e)
            {
                if (!TaakExists(id))
                {
                    return NotFound("Taak was not found");
                }
                else
                {
                    return BadRequest(e.Message);
                }
            }

            return NoContent();
        }

        private bool TaakExists(int id)
        {
            return _taakService.TaakExists(id);
        }

        private bool PersoonExists(int id)
        {
            return _taakService.PersoonExists(id);
        }
    }
}
