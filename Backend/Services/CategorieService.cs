﻿using Backend.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class CategorieService : ICategorieService
    {
        private readonly BarometerContext _dbContext;
        private string errorMessage { get; set; }

        public CategorieService()
        {
            var option = new DbContextOptionsBuilder<BarometerContext>().UseMySql("Server=edu3.mysql.database.azure.com;Port=3306;Uid=edu3admin;Pwd=agile2022!;Database=mydb;", new MariaDbServerVersion(new Version(10, 4, 12)));
            _dbContext = new BarometerContext(option.Options);
        }

        public IEnumerable<Categorie> AlleCategorien()
        {
            return _dbContext.Categorie.ToList();
        }

        public Categorie VindCategorie(int id)
        {
            return _dbContext.Categorie.FirstOrDefault(x => x.Id == id);
        }

        public void PutCategorie(int id, Categorie categorie)
        {
            if (CategorieHasError(categorie))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                _dbContext.Entry(categorie).State = EntityState.Modified;
                _dbContext.SaveChangesAsync();
            }
        }

        public void PostCategorie(Categorie categorie)
        {
            if (CategorieHasError(categorie))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                var cat = _dbContext.Categorie.Any(e => e.Naam == categorie.Naam);
                if (cat)
                {
                    errorMessage = "This categorie already exists";
                    throw new ArgumentException(errorMessage);
                }
                else
                {
                    _dbContext.Categorie.Add(categorie);
                    _dbContext.SaveChangesAsync();
                }              
            }  
        }

        public void DeleteCategorie(int id)
        {
            var categorie = _dbContext.Categorie.FirstOrDefault(x => x.Id == id);

            if (categorie != null)
            {
                _dbContext.Categorie.Remove(categorie);
                _dbContext.SaveChangesAsync();
            } else
            {
                throw new Exception("Categorie was not found");
            }
        }

        public bool CategorieExists(int id)
        {
            return _dbContext.Categorie.Any(e => e.Id == id);
        }

        public bool CategorieHasError(Categorie c)
        {
            if (String.IsNullOrEmpty(c.Naam))
            {
                errorMessage = "Naam should not be empty";
                return true;
            } else if (String.IsNullOrEmpty(c.Kleur))
            {
                errorMessage = "Kleur should not be empty";
                return true;
            }

            return false;
        }
  
    }
    public interface ICategorieService
    {
        IEnumerable<Categorie> AlleCategorien();
        Categorie VindCategorie(int id);
        void PutCategorie(int id, Categorie categorie);
        void PostCategorie(Categorie categorie);
        void DeleteCategorie(int id);
        bool CategorieExists(int id);
    }
}
