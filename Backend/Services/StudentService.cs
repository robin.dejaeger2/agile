﻿using Backend.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class StudentService : IStudentService
    {
        private readonly BarometerContext _dbContext;
        private string errorMessage { get; set; }

        public StudentService()
        {
            var option = new DbContextOptionsBuilder<BarometerContext>().UseMySql("Server=edu3.mysql.database.azure.com;Port=3306;Uid=edu3admin;Pwd=agile2022!;Database=mydb;", new MariaDbServerVersion(new Version(10, 4, 12)));
            _dbContext = new BarometerContext(option.Options);
        }

        public IEnumerable<Student> AlleStudenten()
        {
            return _dbContext.Student.ToList();
        }

        public Student VindStudent(int id)
        {
            return _dbContext.Student.FirstOrDefault(x => x.Id == id);
        }

        public void PutStudent(int id, Student student)
        {
            if (StudentHasError(student))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                _dbContext.Entry(student).State = EntityState.Modified;
                _dbContext.SaveChangesAsync();
            } 
        }

        public void PostStudent(Student student)
        {
            if (StudentHasError(student))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                _dbContext.Student.Add(student);
                _dbContext.SaveChangesAsync();
            }
            
        }

        public void DeleteStudent(int id)
        {
            var student = _dbContext.Student.FirstOrDefault(x => x.Id == id);

            if (student != null)
            {
                _dbContext.Student.Remove(student);
                _dbContext.SaveChanges();
            }
            else
            {
                throw new Exception("Student was not found");
            }
        }

        public bool StudentExists(int id)
        {
            return _dbContext.Student.Any(e => e.Id == id);
        }

        public bool StudentHasError(Student s)
        {
            return false;
        }
    }

    public interface IStudentService
    {
        IEnumerable<Student> AlleStudenten();
        Student VindStudent(int id);
        void PutStudent(int id, Student student);
        void PostStudent(Student student);
        void DeleteStudent(int id);
        bool StudentExists(int id);
    }
}
