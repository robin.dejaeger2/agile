﻿using Backend.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class DocentService : IDocentService
    {
        private readonly BarometerContext _dbContext;
        private string errorMessage { get; set; }

        public DocentService()
        {
            var option = new DbContextOptionsBuilder<BarometerContext>().UseMySql("Server=edu3.mysql.database.azure.com;Port=3306;Uid=edu3admin;Pwd=agile2022!;Database=mydb;", new MariaDbServerVersion(new Version(10, 4, 12)));
            _dbContext = new BarometerContext(option.Options);
        }

        public IEnumerable<Docent> AlleDocenten()
        {
            return _dbContext.Docent.ToList();
        }

        public Docent VindDocent(int id)
        {
            return _dbContext.Docent.FirstOrDefault(x => x.Id == id);
        }

        public void PutDocent(int id, Docent docent)
        {
            if (DocentHasError(docent))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                _dbContext.Entry(docent).State = EntityState.Modified;
                _dbContext.SaveChangesAsync();
            }     
        }

        public void PostDocent(Docent docent)
        {
            if (DocentHasError(docent))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                _dbContext.Docent.Add(docent);
                _dbContext.SaveChangesAsync();       
            } 
        }

        public void DeleteDocent(int id)
        {
            var docent = _dbContext.Docent.FirstOrDefault(x => x.Id == id);

            if (docent != null)
            {
                _dbContext.Docent.Remove(docent);
                _dbContext.SaveChangesAsync();
            } else
            {
                throw new Exception("Docent was not found");
            }
        }

        public bool DocentExists(int id)
        {
            return _dbContext.Docent.Any(e => e.Id == id);
        }

        public bool DocentHasError(Docent d)
        {
            return false;
        }
    }

    public interface IDocentService
    {
        IEnumerable<Docent> AlleDocenten();
        Docent VindDocent(int id);
        void PutDocent(int id, Docent docent);
        void PostDocent(Docent docent);
        void DeleteDocent(int id);
        bool DocentExists(int id);
    }
}
