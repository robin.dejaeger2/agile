﻿using Backend.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class DoelstellingService: IDoelstellingService
    {
        private readonly BarometerContext _dbContext;
        private string errorMessage { get; set; }

        public DoelstellingService()
        {
            var option = new DbContextOptionsBuilder<BarometerContext>().UseMySql("Server=edu3.mysql.database.azure.com;Port=3306;Uid=edu3admin;Pwd=agile2022!;Database=mydb;", new MariaDbServerVersion(new Version(10, 4, 12)));
            _dbContext = new BarometerContext(option.Options);
        }

        public IEnumerable<Doelstelling> AlleDoelstellingen()
        {
            return _dbContext.Doelstelling.ToList();
        }

        public string PutDoelstellingen(int id, BehaaldeDoelstellingen behaaldeDoelstellingen)
        {
            var persoon = _dbContext.Persoon.First(x => x.Id == id);
            persoon.BehaaldeDoelstellingen = behaaldeDoelstellingen.Doelstellingen;

            if (DoelstellingHasError(behaaldeDoelstellingen))
            {
                throw new Exception(errorMessage);
            } else
            {
                _dbContext.Entry(persoon).State = EntityState.Modified;
                _dbContext.SaveChangesAsync();
            }
            
            return behaaldeDoelstellingen.Doelstellingen;
        }

        private bool DoelstellingHasError(BehaaldeDoelstellingen behaaldeDoelstellingen)
        {
            foreach (char c in behaaldeDoelstellingen.Doelstellingen)
            {
                if (c != '0' && c != '1')
                {
                    errorMessage = "Behaalde doelstellingen contains invalid character";
                    return true;
                }
            }

            return false;
        }

        public bool PersoonExists(int id)
        {
            return _dbContext.Persoon.Any(e => e.Id == id);
        }
    }

    public interface IDoelstellingService
    {
        IEnumerable<Doelstelling> AlleDoelstellingen();
        string PutDoelstellingen(int id, BehaaldeDoelstellingen behaaldeDoelstellingen);
        bool PersoonExists(int id);

    }
}
