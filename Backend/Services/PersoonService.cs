﻿using Backend.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class PersoonService : IPersoonService
    {
        private readonly BarometerContext _dbContext;
        private string errorMessage { get; set; }

        public PersoonService() 
        {
            var option = new DbContextOptionsBuilder<BarometerContext>().UseMySql("Server=edu3.mysql.database.azure.com;Port=3306;Uid=edu3admin;Pwd=agile2022!;Database=mydb;", new MariaDbServerVersion(new Version(10, 4, 12)));
            _dbContext = new BarometerContext(option.Options);
        }

        public IEnumerable<Persoon> AllePersonen()
        {
            return _dbContext.Persoon.ToList();
        }

        public Persoon VindPersoon(int id)
        {
            return _dbContext.Persoon.First(x => x.Id == id);
        }

        public void PutPersoon(int id, Persoon persoon)
        {
            if (PersoonHasError(persoon))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                _dbContext.Entry(persoon).State = EntityState.Modified;
                _dbContext.SaveChangesAsync();
            }         
        }

        public void PostPersoon(Persoon persoon)
        {
            if (PersoonHasError(persoon))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                var p = _dbContext.Persoon.Any(e => e.Email == persoon.Email);
                if (p)
                {
                    errorMessage = "This persoon already exists";
                    throw new ArgumentException(errorMessage);
                } else
                {
                    _dbContext.Persoon.Add(persoon);
                    _dbContext.SaveChangesAsync();
                }                
            } 
        }

        public void DeletePersoon(int id)
        {
            var persoon = _dbContext.Persoon.FirstOrDefault(x => x.Id == id);

            if (persoon != null)
            {
                _dbContext.Persoon.Remove(persoon);
                _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new Exception("Persoon was not found");
            }
        }

        public bool PersoonExists(int id)
        {
            return _dbContext.Persoon.Any(e => e.Id == id);
        }

        public bool PersoonHasError(Persoon p)
        {
            if (String.IsNullOrEmpty(p.Naam))
            {
                errorMessage = "Naam should not be empty";
                return true;
            }
            else if (String.IsNullOrEmpty(p.Voornaam))
            {
                errorMessage = "Voornaam should not be empty";
                return true;
            }
            else if (!ValidEmail(p.Email))
            {
                errorMessage = "Email is not valid";
                return true;
            } 
            else if (p.Paswoord.Length < 6){
                errorMessage = "Paswoord is too short";
                return true;
            } 
            else if (WrongID(p))
            {
                return true;
            }
            else if (NotValidRole(p.Student_id, p.Docent_id, p.Mentor_id))
            {
                errorMessage = "Persoon can only be one of three: student, docent or mentor";
                return true;
            }
            else if (IDNotExisting(p))
            {
                return true;
            }

            return false;
        }

        public bool IDNotExisting(Persoon p)
        {
            if(p.Stageplaats_id != null)
            {
                var stageplaats = _dbContext.Stageplaats.First(x => x.Id == p.Stageplaats_id);
                if(stageplaats == null)
                {
                    errorMessage = "Stageplaats does not exist";
                    return true;
                }
            }
            if (p.Student_id != null)
            {
                var student = _dbContext.Student.First(x => x.Id == p.Student_id);
                if (student == null)
                {
                    errorMessage = "Student does not exist";
                    return true;
                }
            }
            if (p.Docent_id != null)
            {
                var docent = _dbContext.Docent.First(x => x.Id == p.Docent_id);
                if (docent == null)
                {
                    errorMessage = "Docent does not exist";
                    return true;
                }
            }
            if (p.Mentor_id != null)
            {
                var mentor = _dbContext.Mentor.First(x => x.Id == p.Mentor_id);
                if (mentor == null)
                {
                    errorMessage = "Mentor does not exist";
                    return true;
                }
            }

            return false;
        }

        public bool WrongID(Persoon p)
        {
            if(!(int.TryParse("" + p.Stageplaats_id, out _)))
            {
                errorMessage = "Stageplaats has to be numeric";
                return true;
            }
            else if (!(int.TryParse("" + p.Student_id, out _)))
            {
                errorMessage = "Student has to be numeric";
                return true;
            }
            else if (!(int.TryParse("" + p.Docent_id, out _)))
            {
                errorMessage = "Docent has to be numeric";
                return true;
            }
            else if (!(int.TryParse("" + p.Mentor_id, out _)))
            {
                errorMessage = "Mentor has to be numeric";
                return true;
            } else
            {
                return false;
            }

        }

        public bool NotValidRole(int? student, int? docent, int? mentor)
        {
            int count = 0;

            if(student != null)
            {
                count += 1;
            }
            if (docent != null)
            {
                count += 1;
            }
            if (mentor != null)
            {
                count += 1;
            }

            if(count > 1)
            {
                return true;
            } else
            {
                return false;
            }
        }

        public bool ValidEmail(string email)
        {
            var trimmedEmail = email.Trim();

            if (trimmedEmail.EndsWith("."))
            {
                return false;
            }
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == trimmedEmail;
            }
            catch
            {
                return false;
            }
        }
    }

    public interface IPersoonService
    {
        IEnumerable<Persoon> AllePersonen();
        Persoon VindPersoon(int id);
        void PutPersoon(int id, Persoon persoon);
        void PostPersoon(Persoon persoon);
        void DeletePersoon(int id);
        bool PersoonExists(int id);
    }
}
