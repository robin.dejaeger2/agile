﻿using Backend.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class TaakService : ITaakService
    {
        private readonly BarometerContext _dbContext;
        private string errorMessage { get; set; }

        public TaakService()
        {
            var option = new DbContextOptionsBuilder<BarometerContext>().UseMySql("Server=edu3.mysql.database.azure.com;Port=3306;Uid=edu3admin;Pwd=agile2022!;Database=mydb;", new MariaDbServerVersion(new Version(10, 4, 12)));
            _dbContext = new BarometerContext(option.Options);
        }
        public object TakenStudent(int id, string datum)
        {
            DateTime date;
            bool isDateTime = false;

            isDateTime = DateTime.TryParse(datum, out date);

            if (isDateTime)
            {
                try
                {
                    var takenDB = _dbContext.Taak.Where(t => t.Datum == date).Where(t => t.Persoon_id == id).ToList();

                    var taken = new List<object>();

                    foreach (Taak t in takenDB)
                    {
                        var evaluaties = _dbContext.Evaluatie.Where(e => e.Taak_id == t.Id).ToList();
                        taken.Add(new
                        {
                            id = t.Id,
                            titel = t.Titel,
                            beschrijving = t.Beschrijving,
                            categore = t.Categorie,
                            datum = t.Datum,
                            duur = t.Duur,
                            evaluaties = evaluaties
                        });
                    }

                    return taken;
                } catch(Exception e)
                {
                    throw new Exception(e.Message);
                }
            } else
            {
                throw new ArgumentException("Invalid Date");
            }
        }

        public object AlleTaken(int id)
        {
            try
            {
                var takenDB = _dbContext.Taak.Where(t => t.Persoon_id == id).ToList();
                var taken = new List<object>();

                foreach (Taak t in takenDB)
                {
                    var evaluaties = _dbContext.Evaluatie.Where(e => e.Taak_id == t.Id).ToList();
                    taken.Add(new
                    {
                        id = t.Id,
                        titel = t.Titel,
                        beschrijving = t.Beschrijving,
                        categore = t.Categorie,
                        datum = t.Datum,
                        duur = t.Duur,
                        evaluaties = evaluaties
                    });
                }

                return taken;
            }
            catch(Exception e)
            {
                throw new ArgumentException(e.Message);
            }
        }

        public Taak VindTaak(int id)
        {
            return _dbContext.Taak.FirstOrDefault(x => x.Id == id);
        }

        public void PutTaak(int id, Taak taak)
        {
            if (TaakHasError(taak))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                _dbContext.Entry(taak).State = EntityState.Modified;
                _dbContext.SaveChangesAsync();
            }    
        }

        public Taak PostTaak(Taak taak)
        {
            if (TaakHasError(taak))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                _dbContext.Taak.Add(taak);
                _dbContext.SaveChangesAsync();

                return taak;
            } 
        }

        public void DeleteTaak(int id)
        {
            var taak = _dbContext.Taak.FirstOrDefault(x => x.Id == id);

            if (taak != null)
            {
                _dbContext.Taak.Remove(taak);
                _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new Exception("Taak was not found");
            }
        }

        public bool TaakExists(int id)
        {
            return _dbContext.Taak.Any(e => e.Id == id);
        }

        public bool PersoonExists(int id)
        {
            return _dbContext.Persoon.Any(e => e.Id == id);
        }

        public bool TaakHasError(Taak taak)
        {
            if (BasicInfoHasError(taak))
            {
                return true;
            }

            if (!PersoonExists(taak.Persoon_id))
            {
                errorMessage = "Persoon does not exist";
                return true;
            }

            return false;
        }

        public bool BasicInfoHasError(Taak taak)
        {
            if (InputIsEmpty(taak))
            {
                return true;
            }

            if (InputContainsInvalidCharacter(taak))
            {
                return true;
            }

            return false;
        }

        public bool InputContainsInvalidCharacter(Taak t)
        {
            List<string> invalidChars = new List<string>() { "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-" };

            foreach (string s in invalidChars)
            {
                if (t.Titel.Contains(s))
                {
                    errorMessage = "Titel has invalid character";
                    return false;
                }

                if (t.Beschrijving.Contains(s))
                {
                    errorMessage = "Beschrijving has invalid character";
                    return false;
                }

                if (t.Categorie.Contains(s))
                {
                    errorMessage = "Categorie has invalid character";
                    return false;
                }
            }

            return false;
        }

        public bool InputIsEmpty(Taak t)
        {
            if (String.IsNullOrEmpty(t.Titel))
            {
                errorMessage = "Titel should not be empty";
                return true;
            }

            if (String.IsNullOrEmpty(t.Beschrijving))
            {
                errorMessage = "Beschrijving should not be empty";
                return true;
            }

            if (String.IsNullOrEmpty(t.Categorie))
            {
                errorMessage = "Categorie should not be empty";
                return true;
            }

            return false;
        }
    }
    public interface ITaakService
    {
        object TakenStudent(int id, string datum);
        object AlleTaken(int id);
        Taak VindTaak(int id);
        void PutTaak(int id, Taak taak);
        Taak PostTaak(Taak taak);
        void DeleteTaak(int id);
        bool TaakExists(int id);
        bool PersoonExists(int id);

    }
}
