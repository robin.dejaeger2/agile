﻿using Backend.Helpers;
using Backend.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Backend.Services
{
    //https://jasonwatmore.com/post/2021/04/30/net-5-jwt-authentication-tutorial-with-example-api#users-controller-cs
    public class LoginService : ILoginService
    {
        private readonly BarometerContext _dbContext;
        private readonly AppSettings _appSettings;
        private string errorMessage { get; set; }

        public LoginService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
            var option = new DbContextOptionsBuilder<BarometerContext>().UseMySql("Server=edu3.mysql.database.azure.com;Port=3306;Uid=edu3admin;Pwd=agile2022!;Database=mydb;", new MariaDbServerVersion(new Version(10, 4, 12)));
            _dbContext = new BarometerContext(option.Options);
        }

        public AuthenticateResponse Authenticate(AuthenticateRequest model)
        {
            if (AuthenticateHasError(model))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                try
                {
                    var user = _dbContext.Persoon.First(p => p.Email == model.Email && p.Paswoord == model.Paswoord);
                    // return null if user not found
                    if (user == null) return null;

                    // authentication successful so generate jwt token
                    var token = generateJwtToken(user);

                    return new AuthenticateResponse(user, token);
                }
                catch
                {
                    return null;
                }
            }  
        }

        public async Task<AuthenticateResponse> SSO(SSORequest model)
        {
            if (SSOHasError(model))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                try
                {
                    var user = _dbContext.Persoon.First(p => p.Email == model.Email);
                    var token = generateJwtToken(user);

                    return new AuthenticateResponse(user, token);
                }
                catch
                {
                    RegisterRequest req = new()
                    {
                        Naam = model.Naam,
                        Voornaam = model.Voornaam,
                        Email = model.Email,
                        Rol = "Student",
                        Paswoord = "Secret123",
                        Code = null
                    };

                    IRegisterService registerService = new RegisterService();
                    var user = await registerService.Register(req);
                    var token = generateJwtToken(user);

                    return new AuthenticateResponse(user, token);
                }
            }         
        }

        public async Task<AuthenticateResponse> GetPersonInfo(int id)
        {
            if (GetPersonInfoHasError(id))
            {
                throw new ArgumentException(errorMessage);
            }
            else
            {
                try
                {
                    var user = _dbContext.Persoon.First(p => p.Id == id);
                    var token = generateJwtToken(user);

                    return new AuthenticateResponse(user, token);
                }
                catch
                {
                    return null;
                }
            }    
        }

        private string generateJwtToken(Persoon persoon)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", persoon.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public Persoon GetById(int id)
        {
            return _dbContext.Persoon.FirstOrDefault(x => x.Id == id);
        }

        public object GetStageplaats(int? id)
        {
            var stageplaats = _dbContext.Stageplaats.FirstOrDefault(x => x.Id == id);
            if(stageplaats != null)
            {
                return new
                {
                    label = "workplace",
                    id = stageplaats.Id,
                    name = stageplaats.Naam,
                    gsm = stageplaats.Gsm,
                    address = stageplaats.Straat + " " + stageplaats.Huisnummer + ", " + stageplaats.Postcode + " " + stageplaats.Gemeente
                };
            } else
            {
                return new { };
            }
            
        }

        public List<object> GetDocenten(int? id)
        {
            List<int> docentenids = _dbContext.Student_Heeft_Docent.Where(x => x.Student_id == id).Select(x => x.Docent_id).ToList();
            if(docentenids != null)
            {
                var docentenList = _dbContext.Persoon.Where(t => docentenids.Contains((int)t.Docent_id)).ToList();
                var docenten = new List<Object>();
                foreach(Persoon d in docentenList)
                {
                    docenten.Add(new { label = "docent", id = d.Docent_id, name = d.Naam + " " + d.Voornaam, email = d.Email });
                }
                return docenten;
            } else
            {
                return new List<object>();
            }
        }

        public List<object> GetMentors(int? id)
        {
            List<int> mentorsids = _dbContext.Student_Heeft_Mentor.Where(x => x.Student_id == id).Select(x => x.Mentor_id).ToList();
            if (mentorsids != null)
            {
                var mentorsList = _dbContext.Persoon.Where(t => mentorsids.Contains((int)t.Mentor_id)).ToList();
                var mentors = new List<Object>();
                foreach (Persoon m in mentorsList)
                {
                    mentors.Add(new { label = "mentor", id = m.Mentor_id, name = m.Naam + " " + m.Voornaam, email = m.Email });
                }
                return mentors;
            }
            else
            {
                return new List<object>();
            }
        }

        public List<object> GetStudentsOfDocent(int? id)
        {
            List<int> studentsids = _dbContext.Student_Heeft_Docent.Where(x => x.Docent_id == id).Select(x => x.Student_id).ToList();
            return GetStudents(studentsids);
        }

        public List<object> GetStudentsOfMentor(int? id)
        {
            List<int> studentsids = _dbContext.Student_Heeft_Mentor.Where(x => x.Mentor_id == id).Select(x => x.Student_id).ToList();
            return GetStudents(studentsids);
        }

        public List<object> GetStudents(List<int> studentsids)
        {
            if (studentsids != null)
            {
                var studentsList = _dbContext.Persoon.Where(t => studentsids.Contains((int)t.Student_id)).ToList();
                var students = new List<Object>();
                foreach (Persoon s in studentsList)
                {
                    students.Add(new { label = "student", persoon_id = s.Id, student_id = s.Student_id, name = s.Naam + " " + s.Voornaam, email = s.Email, behaaldeDoelstellingen = s.BehaaldeDoelstellingen });
                }
                return students;
            }
            else
            {
                return new List<object>();
            }
        }

        public bool AuthenticateHasError(AuthenticateRequest model)
        {
            if (String.IsNullOrEmpty(model.Paswoord))
            {
                errorMessage = "Paswoord should not be empty";
                return true;
            }

            if (String.IsNullOrEmpty(model.Email))
            {
                errorMessage = "Email should not be empty";
                return true;
            }

            if (!ValidEmail(model.Email))
            {
                errorMessage = "Email is not valid";
                return true;
            }

            return false;
        }

        public bool SSOHasError(SSORequest model)
        {
            List<string> invalidChars = new List<string>() { "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-" };

            if (!ValidNaam(model.Naam, invalidChars))
            {
                errorMessage = "Naam has invalid characters";
                return true;
            }

            if (!ValidVoornaam(model.Voornaam, invalidChars))
            {
                errorMessage = "Voornaam has incorrect characters";
                return true;
            }

            if (String.IsNullOrEmpty(model.Naam))
            {
                errorMessage = "Naam should not be empty";
                return true;
            }

            if (String.IsNullOrEmpty(model.Voornaam))
            {
                errorMessage = "Voornaam should not be empty";
                return true;
            }

            if (String.IsNullOrEmpty(model.Email))
            {
                errorMessage = "Email should not be empty";
                return true;
            }

            if (!ValidEmail(model.Email))
            {
                errorMessage = "Email is not valid";
                return true;
            }

            return false;
        }

        public bool GetPersonInfoHasError(int id)
        {
            var user = _dbContext.Persoon.First(p => p.Id == id);

            if(user == null)
            {
                errorMessage = "Persoon does not exist";
                return true;
            }

            return false;
        }


        public bool ValidNaam(string naam, List<string> invalidChars)
        {

            foreach (string s in invalidChars)
            {
                if (naam.Contains(s))
                {
                    return false;
                }
            }

            return true;
        }

        public bool ValidVoornaam(string voornaam, List<string> invalidChars)
        {
            foreach (string s in invalidChars)
            {
                if (voornaam.Contains(s))
                {
                    return false;
                }
            }
            return true;
        }

        public bool ValidEmail(string email)
        {
            var trimmedEmail = email.Trim();

            if (trimmedEmail.EndsWith("."))
            {
                return false;
            }
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == trimmedEmail;
            }
            catch
            {
                return false;
            }
        }
    }

    public interface ILoginService
    {
        AuthenticateResponse Authenticate(AuthenticateRequest model);
        Task<AuthenticateResponse> SSO(SSORequest model);
        Task<AuthenticateResponse> GetPersonInfo(int id);
        Persoon GetById(int id);
        List<object> GetDocenten(int? student_id);
        List<object> GetMentors(int? student_id);
        object GetStageplaats(int? stageplaats_id);
        List<object> GetStudentsOfDocent(int? docent_id);
        List<object> GetStudentsOfMentor(int? mentor_id);
    }
}
