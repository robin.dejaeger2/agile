﻿using Backend.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class StudentRelatieService : IStudentRelatieService
    {
        private readonly BarometerContext _dbContext;
        public StudentRelatieService()
        {
            var option = new DbContextOptionsBuilder<BarometerContext>().UseMySql("Server=edu3.mysql.database.azure.com;Port=3306;Uid=edu3admin;Pwd=agile2022!;Database=mydb;", new MariaDbServerVersion(new Version(10, 4, 12)));
            _dbContext = new BarometerContext(option.Options);
        }

        public async Task<int> PostRelatieDocent(RequestStudentDocent relatie)
        {
            if (IdExistsDocent(relatie))
            {
                if (NoRelationDocent(relatie))
                {
                    StudentHeeftDocent sd = new StudentHeeftDocent()
                    {
                        Student_id = relatie.Student_id,
                        Docent_id = relatie.Docent_id
                    };

                    _dbContext.Student_Heeft_Docent.Add(sd);
                    await _dbContext.SaveChangesAsync();
                    return relatie.Persoon_id;
                } else
                {
                    throw new Exception("This relation already exists");
                }
            } else
            {
                throw new Exception("Id's don't exist");
            }
        }

        public async Task<int> PostRelatieMentor(RequestStudentMentor relatie)
        {
            if (IdExistsMentor(relatie))
            {
                if (NoRelationMentor(relatie))
                {
                    StudentHeeftMentor sm = new StudentHeeftMentor()
                    {
                        Student_id = relatie.Student_id,
                        Mentor_id = relatie.Mentor_id
                    };

                    _dbContext.Student_Heeft_Mentor.Add(sm);
                    await _dbContext.SaveChangesAsync();

                    return relatie.Persoon_id;
                }  else
                {
                    throw new Exception("This relation already exists");
                }
            }
            else
            {
                throw new Exception("Id's don't exist");
            }

        }

        public bool NoRelationDocent(RequestStudentDocent relatie)
        {
            try
            {
                var result = _dbContext.Student_Heeft_Docent.Where(x => x.Docent_id == relatie.Docent_id).ToList();
                foreach(StudentHeeftDocent sd in result)
                {
                    if(sd.Student_id == relatie.Student_id)
                    {
                        return false;
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool NoRelationMentor(RequestStudentMentor relatie)
        {
            try
            {
                var result = _dbContext.Student_Heeft_Mentor.Where(x => x.Mentor_id == relatie.Mentor_id).ToList();
                foreach (StudentHeeftMentor sm in result)
                {
                    if (sm.Student_id == relatie.Student_id)
                    {
                        return false;
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool IdExistsDocent(RequestStudentDocent relatie)
        {
            try
            {
                var student = _dbContext.Student.Where(x => x.Id == relatie.Student_id).First();
                var docent = _dbContext.Docent.Where(x => x.Id == relatie.Docent_id).First();
                return true;
            } catch
            {
                return false;
            }
        }

        public bool IdExistsMentor(RequestStudentMentor relatie)
        {
            try
            {
                var student = _dbContext.Student.Where(x => x.Id == relatie.Student_id).First();
                var mentor = _dbContext.Mentor.Where(x => x.Id == relatie.Mentor_id).First();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }

    public interface IStudentRelatieService
    {
        Task<int> PostRelatieDocent(RequestStudentDocent relatie);
        Task<int> PostRelatieMentor(RequestStudentMentor relatie);

    }
}
