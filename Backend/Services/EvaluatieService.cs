﻿using Backend.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class EvaluatieService : IEvaluatieService
    {
        private readonly BarometerContext _dbContext;
        private string errorMessage { get; set; }

        public EvaluatieService()
        {
            var option = new DbContextOptionsBuilder<BarometerContext>().UseMySql("Server=edu3.mysql.database.azure.com;Port=3306;Uid=edu3admin;Pwd=agile2022!;Database=mydb;", new MariaDbServerVersion(new Version(10, 4, 12)));
            _dbContext = new BarometerContext(option.Options);
        }

        public IEnumerable<Evaluatie> AlleEvaluaties()
        {
            return _dbContext.Evaluatie.ToList();
        }

        public Evaluatie VindEvaluatie(int id)
        {
            return _dbContext.Evaluatie.FirstOrDefault(x => x.Id == id);
        }

        public void PutEvaluatie(int id, Evaluatie evaluatie)
        {
            if (EvaluatieHasError(evaluatie))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                _dbContext.Entry(evaluatie).State = EntityState.Modified;
                _dbContext.SaveChangesAsync();
            }  
        }

        public bool EvaluatieHasError(Evaluatie e)
        {
            if (String.IsNullOrEmpty(e.Feedback))
            {
                errorMessage = "Feedback should not be empty";
                return true;
            }

            var taak = _dbContext.Taak.FirstOrDefault(x => x.Id == e.Taak_id);
            if (taak == null)
            {
                errorMessage = "Taak does not exist";
                return true;
            }

            return false;
        }

        public async Task<object> PostEvaluatie(EvaluatieRequest evaluatie)
        {
            Evaluatie ev = new Evaluatie()
            {
                Complexiteit = evaluatie.Complexiteit,
                Begeleiding = evaluatie.Begeleiding,
                Voldoening = evaluatie.Voldoening,
                Feedback = evaluatie.Feedback,
                Taak_id = evaluatie.Taak_id
            };

            if (EvaluatieHasError(ev))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                try
                {
                    _dbContext.Evaluatie.Add(ev);
                    await _dbContext.SaveChangesAsync();

                    var t = _dbContext.Taak.Where(x => x.Id == evaluatie.Taak_id).First();

                    if(evaluatie.Duur != null)
                    {
                        t.Duur = evaluatie.Duur;
                        _dbContext.Entry(t).State = EntityState.Modified;
                        await _dbContext.SaveChangesAsync();
                    }

                    var obj = new List<object>();

                    var evaluaties = _dbContext.Evaluatie.Where(e => e.Taak_id == t.Id).ToList();
                    obj.Add(new
                    {
                        id = t.Id,
                        titel = t.Titel,
                        beschrijving = t.Beschrijving,
                        categore = t.Categorie,
                        datum = t.Datum,
                        duur = t.Duur,
                        evaluaties = evaluaties
                    });

                    return obj;
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }
            }
            
        }

        public void DeleteEvaluatie(int id)
        {
            var evaluatie = _dbContext.Evaluatie.FirstOrDefault(x => x.Id == id);

            if (evaluatie != null)
            {
                _dbContext.Evaluatie.Remove(evaluatie);
                _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new Exception("Evaluatie was not found");
            }
        }

        public bool EvaluatieExists(int id)
        {
            return _dbContext.Evaluatie.Any(e => e.Id == id);
        }
    }

    public interface IEvaluatieService
    {
        IEnumerable<Evaluatie> AlleEvaluaties();
        Evaluatie VindEvaluatie(int id);
        void PutEvaluatie(int id, Evaluatie evaluatie);
        Task<object> PostEvaluatie(EvaluatieRequest evaluatie);
        void DeleteEvaluatie(int id);
        bool EvaluatieExists(int id);
    }
}
