﻿using Backend.Helpers;
using Backend.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class RegisterService : IRegisterService
    {
        private readonly BarometerContext _dbContext;
        private string message { get; set; }

        public RegisterService()
        {
            var option = new DbContextOptionsBuilder<BarometerContext>().UseMySql("Server=edu3.mysql.database.azure.com;Port=3306;Uid=edu3admin;Pwd=agile2022!;Database=mydb;", new MariaDbServerVersion(new Version(10, 4, 12)));
            _dbContext = new BarometerContext(option.Options);
        }

        public async Task<Persoon> Register(RegisterRequest model)
        {
            Persoon persoon = new();

            bool error = WrongInput(model);

            if (error)
            {
               throw new ArgumentException(message);
            }

            persoon.Naam = model.Naam;
            persoon.Voornaam = model.Voornaam;
            persoon.Email = model.Email;
            persoon.Paswoord = model.Paswoord;
            persoon.Stageplaats_id = null;
            persoon.BehaaldeDoelstellingen = "000000";

            if (model.Rol.Equals("Student"))
            {
                Student student = new(){ };
                _dbContext.Student.Add(student);
                await _dbContext.SaveChangesAsync();

                persoon.Student_id = student.Id;
                persoon.Docent_id = null;
                persoon.Mentor_id = null;
            }
            else if(IsAllowed(model.Code))
            {
                if (model.Rol.Equals("Docent"))
                {
                    Docent docent = new() { };
                    _dbContext.Docent.Add(docent);
                    await _dbContext.SaveChangesAsync();

                    persoon.Student_id = null;
                    persoon.Docent_id = docent.Id;
                    persoon.Mentor_id = null;
                }
                else
                {
                    Mentor mentor = new() { };
                    _dbContext.Mentor.Add(mentor);
                    await _dbContext.SaveChangesAsync();

                    persoon.Student_id = null;
                    persoon.Docent_id = null;
                    persoon.Mentor_id = mentor.Id;
                }
            } else
            {
                throw new Exception(message);
            }

            _dbContext.Persoon.Add(persoon);
            await _dbContext.SaveChangesAsync();

            return persoon;
        }

        public bool IsAllowed(string code)
        {
            var secret = _dbContext.Code.First();
            message = "Wrong code";
            return code.Equals(secret.Code);
        }

        public bool WrongInput(RegisterRequest model)
        {
            List<string> invalidChars = new List<string>() { "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-" };

            if (!ValidNaam(model.Naam, invalidChars))
            {
                message = "Naam has invalid characters";
                return true;
            }

            if (!ValidVoornaam(model.Voornaam, invalidChars))
            {
                message = "Voornaam has incorrect characters";
                return true;
            }

            if (!model.Rol.Equals("Student") && !model.Rol.Equals("Docent") && !model.Rol.Equals("Mentor"))
            {
                message = "Rol should be Student, Docent or Mentor";
                return true;
            }

            if (!ValidEmail(model.Email))
            {
                message = "Email is not valid";
                return true;
            }

            if(model.Paswoord.Length < 6)
            {
                message = "Paswoord is to short";
                return true;
            }

            var p = _dbContext.Persoon.Any(e => e.Email == model.Email);
            if (p)
            {
                message = "This persoon already exists";
                throw new ArgumentException(message);
            }

            return false;
        }

        public bool ValidEmail(string email)
        {
            var trimmedEmail = email.Trim();

            if (trimmedEmail.EndsWith("."))
            {
                return false;
            }
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == trimmedEmail;
            }
            catch
            {
                return false;
            }
        }

        public bool ValidNaam(string naam, List<string> invalidChars)
        {

            foreach (string s in invalidChars)
            {
                if (naam.Contains(s))
                {
                    return false;
                }
            }

            return true;
        }

        public bool ValidVoornaam(string voornaam, List<string> invalidChars)
        {
            foreach (string s in invalidChars)
            {
                if (voornaam.Contains(s))
                {
                    return false;
                }
            }
            return true;
        }
   

    }

    public interface IRegisterService
    {
        Task<Persoon> Register(RegisterRequest model);
    }
}
