﻿using Backend.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class StageplaatsService : IStagePlaatsService
    {
        private readonly BarometerContext _dbContext;
        private string errorMessage { get; set; }

        public StageplaatsService()
        {
            var option = new DbContextOptionsBuilder<BarometerContext>().UseMySql("Server=edu3.mysql.database.azure.com;Port=3306;Uid=edu3admin;Pwd=agile2022!;Database=mydb;", new MariaDbServerVersion(new Version(10, 4, 12)));
            _dbContext = new BarometerContext(option.Options);
        }

        public IEnumerable<Stageplaats> AlleStageplaatsen()
        {
            return _dbContext.Stageplaats.ToList();
        }

        public Stageplaats FindStageplaats(int id)
        {
            return _dbContext.Stageplaats.FirstOrDefault(x => x.Id == id);
        }

        public void PutStageplaats(int id, Stageplaats stageplaats)
        {
            if (StageplaatsHasError(stageplaats))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                _dbContext.Entry(stageplaats).State = EntityState.Modified;
                _dbContext.SaveChangesAsync();
            }      
        }

        public void PostStageplaats(Stageplaats stageplaats)
        {
            if (StageplaatsHasError(stageplaats))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                _dbContext.Stageplaats.Add(stageplaats);
                _dbContext.SaveChangesAsync();
            }      
        }

        public async Task<Stageplaats> PostStageplaatsStudent(StageplaatsRequest model)
        {
            if (StageplaatsReqestHasError(model))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                try
                {
                    Stageplaats s = new()
                    {
                        Naam = model.Naam,
                        Straat = model.Straat,
                        Huisnummer = model.Huisnummer,
                        Gemeente = model.Gemeente,
                        Postcode = model.Postcode,
                        Gsm = model.Gsm,
                    };

                    Persoon p = _dbContext.Persoon.First(x => x.Id == model.Persoon_Id);

                    _dbContext.Stageplaats.Add(s);
                    await _dbContext.SaveChangesAsync();

                    p.Stageplaats_id = s.Id;

                    _dbContext.Entry(p).State = EntityState.Modified;
                    await _dbContext.SaveChangesAsync();

                    return s;
                }
                catch (Exception e)
                {
                    throw new ArgumentException(e.Message);
                }
            }  
        }

        public void DeleteStageplaats(int id)
        {
            var stageplaats = _dbContext.Stageplaats.FirstOrDefault(x => x.Id == id);

            if (stageplaats != null)
            {
                _dbContext.Stageplaats.Remove(stageplaats);
                _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new Exception("Stageplaats was not found");
            }
        }

        public bool StageplaatsExists(int id)
        {
            return _dbContext.Stageplaats.Any(e => e.Id == id);
        }

        public bool StageplaatsHasError(Stageplaats s)
        {
            List<string> invalidChars = new List<string>() { "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-" };

            if (AdresHasError(s))
            {
                return true;
            }

            if (GsmHasError(s, invalidChars))
            {
                return true;
            }

            if (NaamHasError(s, invalidChars))
            {
                return true;
            }

            return false;
        }

        public bool StageplaatsReqestHasError(StageplaatsRequest model)
        {
            List<string> invalidChars = new List<string>() { "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-" };

            Stageplaats s = new()
            {
                Naam = model.Naam,
                Straat = model.Straat,
                Huisnummer = model.Huisnummer,
                Gemeente = model.Gemeente,
                Postcode = model.Postcode,
                Gsm = model.Gsm,
            };

            if (AdresHasError(s))
            {
                return true;
            }

            if (GsmHasError(s, invalidChars))
            {
                return true;
            }

            if (NaamHasError(s, invalidChars))
            {
                return true;
            }

            var persoon = _dbContext.Persoon.FirstOrDefault(x => x.Id == model.Persoon_Id);
            if(persoon == null)
            {
                errorMessage = "Persoon does not exist";
                return true;
            }

            return false;
        }

        public bool AdresHasError(Stageplaats s)
        {
            if (String.IsNullOrEmpty(s.Straat))
            {
                errorMessage = "Straat should not be empty";
                return true;
            }

            if (String.IsNullOrEmpty(s.Huisnummer))
            {
                errorMessage = "Huisnummer should not be empty";
                return true;
            }

            if (String.IsNullOrEmpty(s.Gemeente))
            {
                errorMessage = "Gemeente should not be empty";
                return true;
            }

            if (String.IsNullOrEmpty(s.Postcode))
            {
                errorMessage = "Postcode should not be empty";
                return true;
            }

            return false;
        }

        public bool GsmHasError(Stageplaats s, List<string> invalidChars)
        {
            foreach (string c in invalidChars)
            {
                if (s.Gsm.Contains(c))
                {
                    errorMessage = "Gsm has an invalid character";
                    return true;
                }
            }

            return false;
        }

        public bool NaamHasError(Stageplaats s, List<string> invalidChars)
        {
            foreach (string c in invalidChars)
            {
                if (s.Naam.Contains(c))
                {
                    errorMessage = "Naam has invalid character(s)";
                    return true;
                }
            }

            if (String.IsNullOrEmpty(s.Naam))
            {
                errorMessage = "Naam should not be empty";
                return true;
            }

            return false;
        }
    }

    public interface IStagePlaatsService
    {
        IEnumerable<Stageplaats> AlleStageplaatsen();
        Stageplaats FindStageplaats(int id);
        Task<Stageplaats> PostStageplaatsStudent(StageplaatsRequest model);
        void PutStageplaats(int id, Stageplaats stageplaats);
        void PostStageplaats(Stageplaats stageplaats);
        void DeleteStageplaats(int id);
        bool StageplaatsExists(int id);
    }
}
