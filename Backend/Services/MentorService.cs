﻿using Backend.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Services
{
    public class MentorService : IMentorService
    {
        private readonly BarometerContext _dbContext;
        private string errorMessage { get; set; }

        public MentorService()
        {
            var option = new DbContextOptionsBuilder<BarometerContext>().UseMySql("Server=edu3.mysql.database.azure.com;Port=3306;Uid=edu3admin;Pwd=agile2022!;Database=mydb;", new MariaDbServerVersion(new Version(10, 4, 12)));
            _dbContext = new BarometerContext(option.Options);
        }

        public IEnumerable<Mentor> AlleMentors()
        {
            return _dbContext.Mentor.ToList();
        }

        public Mentor VindMentor(int id)
        {
            return _dbContext.Mentor.FirstOrDefault(x => x.Id == id);
        }

        public void PutMentor(int id, Mentor mentor)
        {
            if (MentorHasError(mentor))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                _dbContext.Entry(mentor).State = EntityState.Modified;
                _dbContext.SaveChangesAsync();
            }    
        }

        public void PostMentor(Mentor mentor)
        {
            if (MentorHasError(mentor))
            {
                throw new ArgumentException(errorMessage);
            } else
            {
                _dbContext.Mentor.Add(mentor);
                _dbContext.SaveChangesAsync();
            }    
        }

        public void DeleteMentor(int id)
        {
            var mentor = _dbContext.Mentor.FirstOrDefault(x => x.Id == id);

            if (mentor != null)
            {
                _dbContext.Mentor.Remove(mentor);
                _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new Exception("Mentor was not found");
            }
        }

        public bool MentorExists(int id)
        {
            return _dbContext.Mentor.Any(e => e.Id == id);
        }

        public bool MentorHasError(Mentor m)
        {
            return false;
        }
    }

    public interface IMentorService
    {
        IEnumerable<Mentor> AlleMentors();
        Mentor VindMentor(int id);
        void PutMentor(int id, Mentor mentor);
        void PostMentor(Mentor mentor);
        void DeleteMentor(int id);
        bool MentorExists(int id);
    }
}
